#!/usr/bin/perl
use strict;
use warnings;

require URI::Find::Schemeless;

use LWP::Simple;
use Email::Valid;
use constant EVENT_TITLE => 0;
use constant EVENT_TIME => 1;
use constant EVENT_LOC => 2;

# super simple argument validation
die "Usage: perl $0 ISSUE\nISSUE is of format 201XsXweekXXissXXX\n" if @ARGV < 1;

my $ISSUE = $ARGV[0];

# this needs to be replaced
my $folder = "/media/6C201B04201AD4C4/Users/Didi/Google Drive 2/CSESoc/2013/Beta/Issues/";
my $dir = $folder."$ISSUE/Edited";
my $final_dir = $folder."$ISSUE/Final Template/";

opendir(DIR, $dir) or die $!;

while (my $file = readdir(DIR)) {
   # We only want files
   next unless (-f "$dir/$file");

   # Use a regular expression to find files ending in .txt
   next unless ($file =~ m/\.gdoc$/);
   
   my $full_path = "$dir"."/"."$file";

   open FILE, "$full_path" or die $!;

   my $website = <FILE>;
  
   $website =~ s/^{\"url\": \"//ig;
   $website =~ s/\", \".*//ig;
   
   close(FILE);


   my $page = get ("$website");
   my @lines = split(/\n/,$page);
   my $text = $lines[18];
  
   $text =~ s/\\n/\n/ig;

   my @text_array = split (/\n/, $text);

   shift @text_array;
   my @plain_array = {};

   my $events_article = 0;
   my $events_header = 0;
   my $events_line = '';

   if ($text_array[0] =~ m/Title\: Upcoming Events/) {
      $events_article = 1;
   }
   

   my $author = "";
   my $title = "";
   my $subtitle = "";
   my $tags = "";
   my $bolded = 0;

   foreach my $element (@text_array) {
      
      if ($element !~ m/\",\"ibi\"\:/) {

         # apply some simple bolding
         if (length($element) <= 120 
            && length($element) > 1 
            && $element !~ m/Author\:|Title\:|Subtitle\:|Tags\:/) {
            
            # will probably need to fix this to allow for descriptions
            # that have less than 120 characters
            if ($events_article == 1 && $element !~ m/\.\!\?/) {
               if ($events_header == 0) {
                  $element =~ s/(.*)/\\events\{$1\}/i;
                  $events_line = $events_line.$element;
                  $events_header++;
               } elsif ($events_header == 1) {
                  $element =~ s/(.*)/\{$1\}/i;
                  $events_line = $events_line.$element;
                  $events_header++;
               } elsif ($events_header == 2) {
                  $element =~ s/(.*)/\{$1\}/i;
                  $events_line = $events_line.$element;
                  push (@plain_array, $events_line);
                  $events_header = 0;
                  $events_line = '';
               } 

            } 
               elsif ($bolded == 0) {
                  $element =~ s/(.*)/\{\\bf $1\}/i;
                  push (@plain_array, $element);
                  $bolded = 1;
               } else {
                  $bolded = 0;

                  push (@plain_array, $element);
               }
            
         } else {
            # obtain all the title stuff
            if ($element =~ m/Author\:.*/){
                 $element =~ s/Author\:\s(.*)/$1/ig; 
                 $author = $element;
                  
            } elsif ($element =~ m/Subtitle\:/){
                 $element =~ s/Subtitle\:\s(.*)/$1/ig;
                 $subtitle = $element;

            } elsif ($element =~ m/Title\:/){
                 $element =~ s/Title\:\s(.*)/$1/ig;
                 $title = $element;

            } elsif ($element =~ m/Tags\:/){
                 $element =~ s/Tags\:\s(.*)/$1/ig;
                 $tags = $element;

            } else {
                  $bolded = 0;
                  push (@plain_array, $element);
            }
           
         }
      } 

      # can't do tilde globally because different for emails/urls
      $element =~ s/\~/\$\\sim\$/g;

      # emails
      my $address = Email::Valid->address($element);

      # put the tilde back in
      if (defined $address) {
        my $t_address = $address;
        $t_address =~ s/\$\\sim\$/\~/g;
        $element =~ s/$address/\\email\{$t_address\}/g;
      }
      # urls
      my @uris = {};
      my $finder = URI::Find::Schemeless->new(sub {
         my ($uri) = shift;
         push @uris, $uri;
      });
      $finder->find(\$element);

      foreach my $u (@uris) {
         my $t_u = $u;
         $t_u =~ s/\$\\sim\$/\~/g;
         $element =~ s/$u/\\url\{$t_u\}/g;
      }

   }
   
   # the first two empty elements exist as separators
   # they are the newlines before the title stuff and after,
   # which has been removed in above
   shift (@plain_array);
   shift (@plain_array);

   # each element is a paragraph
   my $final_text = join("\n",@plain_array);

   # global formatting begin:
   # newlines
   $final_text =~ s/\n/\\newline\n/ig;

   # adding in newlines after events
   $final_text =~ s/(\\events.*)\\newline/$1/ig;

   # sometimes you get junk in between pages, just make them newlines
   $final_text =~ s/HASH\(.*\)/\\newline/ig;

   # and sometimes people just have wrong keyboards
   # unicode for opening quotes (doesn't always work?)
   $final_text =~ s/\x{201C}/\`\`/g;
   $final_text =~ s/\\\"(.*)\\\"/\`\`$1\'\'/g;

   # unicode for closing quotes
   $final_text =~ s/\x{201D}/\'\'/g;

   # unicode for apostrophes
   $final_text =~ s/\x{2018}|\x{2019}/\'/g;

   # unicde for ellipsis
   $final_text =~ s/\x{2026}/\.\.\./g;

   # formating slashes so LaTeX reads them properly
   $final_text =~ s/\$/\\\$/g;

   # gdocs escapes backslashes, gotta fix that
   $final_text =~ s/\\\//\//g;

   # in LaTeX "<" and ">" are math symbols
   $final_text =~ s/(\<|\>)/\$$1\$/g;

   # the all important empersands
   $final_text =~ s/\&/\\\&/g;

   # finally put in the titles and the author again

   if (defined($subtitle)) {
      # \articlehead{tags}{title}{pageref}
      $final_text = "\\articlehead{$tags}{$title}{}\n" .
         "\\subsection*{$subtitle}\n" .
         "\\begin{multicols}{3}\\noindent\n" .
         "$final_text\\newline\n" .
         "\\theauthor{$author}\n" .
         "\\end{multicols}";
   
   } else {
      $final_text = "\\articlehead{$tags}{$title}{}\n" .
         "\\begin{multicols}{3}\\noindent\n" .
         "$final_text\\newline\n" .
         "\\theauthor{$author}\n" .
         "\\end{multicols}";
   }
   #print $final_text;   

   my $new_article_name = $title.".tex";

   $new_article_name =~ s/\s+/\-/g;
   
   $new_article_name = $final_dir.$new_article_name;

   open (my $ARTICLE, ">", $new_article_name)
      or die "cannot open > $new_article_name: $!";

   print $ARTICLE $final_text."\n";
   print "Created: ".$new_article_name."\n";

   close ($ARTICLE);

}
print "\nDon't forget to fill in the reference name! \n";

closedir(DIR);
exit 0;
