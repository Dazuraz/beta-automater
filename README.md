This is a perl parser that converts gdocs to LaTeX of a specific format for CSESoc Beta. 

# Installation: 
* After downloading the file, edit the path to your google drive directory.
* Open a terminal and run "perl Automater.pl _ISSUE_" where ISSUE is the issue of Beta of format 201XsXweekXXissXXX.

# Things to note:
##All articles must be formatted according to this guideline:
* There is a newline before the title.
* The title order goes as
    1. Title:
    2. Subtitle:
    3. Tags:
    4. Author:
* Include a newline after this.
* There must be two newlines at the end of the article.
* The Events article should have descriptions for each event that are no less than 120 characters.

